import cv2
import numpy as np
import time
from matplotlib import pyplot as plt
import socketio
import xlsxwriter
import datetime
# import asyncio
# import websockets

# async def hello(data):
#     print(data)
#     async with websockets.connect("ws://localhost:8000") as websocket:
#         await websocket.send(data)

#Init socket
sio = socketio.Client()

sio.connect('http://localhost:8000')
@sio.event
def connect():
    print("I'm connected!")

net = cv2.dnn.readNet("yolov4-tiny.weights", "yolov4-tiny.cfg")
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
classes = []
with open("coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]
layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3))

#cap = cv2.VideoCapture(0)
# cap = cv2.VideoCapture("rtsp://admin:admin@192.168.100.158:554/video")

cap = cv2.VideoCapture('car_test.mp4')
font = cv2.FONT_HERSHEY_PLAIN
starting_time = time.time()
frame_id = 0
line = [(732,500), (1249,549)]


object_count = 0
car_count = 0
bus_count = 0
if object_count + 1:
    print(object_count)
    # asyncio.run(hello(str(object_count)))

while True:
    object_detected = 0
    _, frame = cap.read()
    #frame = cv2.CAP_PROP_ZOOM(frame,(200,480),(0,600))
    #frame = cv2.resize(frame, (800, 480))
    incount1 = 0
    incount2 = 0
    frame_id += 1


    height, width, channels = frame.shape

    # Detecting objects #use 224 for yolov4 asyura # use 288 best for (1)ASYURA #192 work correctly for last 288

    blob = cv2.dnn.blobFromImage(frame, 0.00392, (608,608), [0, 0, 0, 0], True, crop=False)

    net.setInput(blob)
    outs = net.forward(output_layers)

    # Showing informations on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.2:
                # Object detected

                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2.5)
                y = int(center_y - h / 2.5)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes (boxes, confidences, 0.005, 0,9)
    for i in range(len(boxes)) :
        if i in indexes :
            x, y, w, h = boxes[i]
            label = str(classes[class_ids[i]])
            if 0 < x < 1280 and 415 > y > 410  :
                object_detected =+ 1
                # print("label=" , class_ids[i])

           
            if class_ids[i] == [2]  and 0 < x < 1280 and 415 > y > 410  :
                incount1 =+ 1

            if class_ids[i] == [7] and 0 < x < 1280 and 415 > y > 410  :
                incount2 =+ 1
            #if class_ids[i] == [5] and 347 < x < 636 and 336 > y > 326 :
              #  incount2 =+ 1
            
            confidence = confidences[i]
            color = colors[class_ids[i]]
            #cv2.line(frame,line[0],line[1] ,color,2)
            cv2.rectangle(frame, (0,405), (1280,415), (255,0,0), 2)
            cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
            #cv2.rectangle(frame, line[0], line[1], color,1)
            cv2.circle(frame, (int((w/2) + x), int((h/2)+y)), 2, (0,0,255), -1)
            #cv2.putText(frame, label + " " + str(round(confidence, 2)), (x, y + 30), font, 3, color, 3)
            cv2.putText(frame, label , (x, y + 30), font, 1, color, 2)



   # print (f'jumlah : {object_detected}')
    object_count = object_count + object_detected
    car_count = incount1 + car_count
    bus_count = incount2 + bus_count
    hitungan = format(object_count)
    mobil = format(car_count)
    bus = format(bus_count)

    elapsed_time = time.time() - starting_time
    fps = frame_id / elapsed_time
    cv2.putText(frame, "FPS: " + str(round(fps, 2)), (10, 50), font, 1, (0, 0, 0), 1)
    cv2.putText(frame, "Count :{}"  .format(object_count), (10, 100), font, 1,(0,0,0),1)
    cv2.putText(frame, "CAR ={}" .format(car_count), (10,120),font,1, (0,0,0), 1 )
    cv2.putText(frame, "BUS/TRUCK ={}" .format(bus_count), (10,140),font,1, (0,0,0), 1 )
    if object_detected == 1:
        print('jumlah {}' .format(object_count))

    timestampStr = datetime.datetime.now().strftime("%d-%b-%Y (%H)")
    workbook =xlsxwriter.Workbook(timestampStr + '.xlsx')
    JUMLAH_TOTAL = workbook.add_worksheet ('sheet1(REPORT HARIAN')
    JUMLAH_PERJAM = workbook.add_worksheet('sheet2(REPORT PER JAM)')
    waktu = datetime.datetime.now()
    TOTAL_HARIAN = (
        ['MOBIL', mobil, format(waktu)],
        ['BUS & TRUCK', bus, format(waktu)] 
    )

    row = 0
    col = 0

    for Nama, Jumlah, Keterangan in (TOTAL_HARIAN):
        JUMLAH_TOTAL.write(row, col, Nama)
        JUMLAH_TOTAL.write(row, col + 1, Jumlah)
        JUMLAH_TOTAL.write(row, col +2 , Keterangan)
        row += 1

    workbook.close()
   
    if object_detected == 1:
        # asyncio.run(hello(str(object_count)))
        sio.emit('count', {"count" : object_count})
        print('jumlah {}' .format(object_count))
    if incount1 == 1 :
        sio.emit('car_count', {"car_count" : car_count})
        print('mobil :{}' .format(car_count))
    if incount2 == 1:
        sio.emit('bus_count', {"bus_count" : bus_count})
        print('bus/truck :{}'.format(bus_count) )


    cv2.imshow("Image",frame)
    # plt.imshow(frame)
    # plt.show()
    key = cv2.waitKey(1)
    if key == 27:
        break
cap.release()
cv2.destroyAllWindows()

