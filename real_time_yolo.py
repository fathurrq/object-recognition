import cv2
import numpy as np
import time
#import RPi.GPIO as GPIO
from matplotlib import pyplot as plt


# Load Yolo
#YOLO_ASYURA = OK 288
#(1) ASYURA = OK 224

net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
classes = []
with open("coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]
layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3))

# AKSES GPIO RASPBERRY pi
#GPIO.setmode (GPIO.BCM)
#Simpang 4
#GPIO.setup(20,GPIO.OUT)
#GPIO.setup(21,GPIO.OUT)
#GPIO.setup(26,GPIO.OUT)
#simpang3
#GPIO.setup(13,GPIO.OUT)
#GPIO.setup(16,GPIO.OUT)
#GPIO.setup(19,GPIO.OUT)
#simpang 2
#GPIO.setup(5,GPIO.OUT)
#GPIO.setup(6,GPIO.OUT)
#GPIO.setup(12,GPIO.OUT)
#Simpang 1
#GPIO.setup(25,GPIO.OUT)
#GPIO.setup(23,GPIO.OUT)
#GPIO.setup(24,GPIO.OUT)
# Loading image
cap = cv2.VideoCapture('Road traffic video for object recognition.mp4')
#cv2.CAP_PROP_EXPOSURE(1)
#cap.set(cv2.CAP_PROP_EXPOSURE, 5)
#cap.set(11, 5)
font = cv2.FONT_HERSHEY_PLAIN
starting_time = time.time()
frame_id = 0
#cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 15)
#cap.set (15, 20)
while True:

    _, frame = cap.read()


    frame_id += 1


    object_detected = 0
    count_1 = 0
    count_2 = 0
    count_3 = 0
    count_4 = 0

    height, width, channels = frame.shape

    # Detecting objects #use 224 for yolov4 asyura # use 288 best for (1)ASYURA #192 work correctly for last 288

    blob = cv2.dnn.blobFromImage(frame, 0.00392, (416, 416), [0, 0, 0, 0], True, crop=False)

    net.setInput(blob)
    outs = net.forward(output_layers)

    # Showing informations on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.2:
                # Object detected

                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2.5)
                y = int(center_y - h / 2.5)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes (boxes, confidences, 0.2, 0.2)
    for i in range(len(boxes)) :
        if i in indexes :
            x, y, w, h = boxes[i]
            if x < 258 and y > 149 :
                count_2 +=1
           import cv2
import numpy as np
import time
#import RPi.GPIO as GPIO
from matplotlib import pyplot as plt


# Load Yolo
#YOLO_ASYURA = OK 288
#(1) ASYURA = OK 224

net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
classes = []
with open("coco.names", "r") as f:
    classes = [line.strip() for line in f.readlines()]
layer_names = net.getLayerNames()
output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
colors = np.random.uniform(0, 255, size=(len(classes), 3))

# AKSES GPIO RASPBERRY pi
#GPIO.setmode (GPIO.BCM)
#Simpang 4
#GPIO.setup(20,GPIO.OUT)
#GPIO.setup(21,GPIO.OUT)
#GPIO.setup(26,GPIO.OUT)
#simpang3
#GPIO.setup(13,GPIO.OUT)
#GPIO.setup(16,GPIO.OUT)
#GPIO.setup(19,GPIO.OUT)
#simpang 2
#GPIO.setup(5,GPIO.OUT)
#GPIO.setup(6,GPIO.OUT)
#GPIO.setup(12,GPIO.OUT)
#Simpang 1
#GPIO.setup(25,GPIO.OUT)
#GPIO.setup(23,GPIO.OUT)
#GPIO.setup(24,GPIO.OUT)
# Loading image
cap = cv2.VideoCapture('Road traffic video for object recognition.mp4')
#cv2.CAP_PROP_EXPOSURE(1)
#cap.set(cv2.CAP_PROP_EXPOSURE, 5)
#cap.set(11, 5)
font = cv2.FONT_HERSHEY_PLAIN
starting_time = time.time()
frame_id = 0
#cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 15)
#cap.set (15, 20)
while True:

    _, frame = cap.read()


    frame_id += 1


    object_detected = 0
    count_1 = 0
    count_2 = 0
    count_3 = 0
    count_4 = 0

    height, width, channels = frame.shape

    # Detecting objects #use 224 for yolov4 asyura # use 288 best for (1)ASYURA #192 work correctly for last 288

    blob = cv2.dnn.blobFromImage(frame, 0.00392, (416, 416), [0, 0, 0, 0], True, crop=False)

    net.setInput(blob)
    outs = net.forward(output_layers)

    # Showing informations on the screen
    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.2:
                # Object detected

                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                # Rectangle coordinates
                x = int(center_x - w / 2.5)
                y = int(center_y - h / 2.5)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes (boxes, confidences, 0.2, 0.2)
    for i in range(len(boxes)) :
        if i in indexes :
            x, y, w, h = boxes[i]
            if x < 258 and y > 149 :
                count_2 +=1
            if x < 280 and y < 130  :
                count_3 += 1
            if x > 330  and  y < 129 :
                count_4 += 1
            if x > 330 and y > 173 :
                count_1 += 1

            label = str(classes[class_ids[i]])
            confidence = confidences[i]
            color = colors[class_ids[i]]
            cv2.rectangle(frame, (x, y), (x + w, y + h), color, 2)
            cv2.circle(frame, (int((w/2) + x), int((h/2)+y)), 2, (0,0,255), -1)
            object_detected+=1

            #cv2.putText(frame, label + " " + str(round(confidence, 2)), (x, y + 30), font, 3, color, 3)
            cv2.putText(frame, label , (x, y + 30), font, 1, color, 2)


    elapsed_time = time.time() - starting_time
    fps = frame_id / elapsed_time
    cv2.putText(frame, "FPS: " + str(round(fps, 2)), (10, 50), font, 1, (0, 0, 0), 1)
    #cv2.putText(frame,"simpang 1:" + str(count_1), (10,80), font, 1, (0,0,0), 1)
    #cv2.putText(frame, "simpang 2:" + str(count_2), (10, 110), font, 1, (0, 0, 0), 1)
    #cv2.putText(frame, "simpang 3:" + str(count_3), (10, 140), font, 1, (0, 0, 0), 1)
    #cv2.putText(frame, 'simpang 4:' + str(count_4), (10,170), font, 1, (0,0,0),1)
#simpang 1
    if int(count_1) == 0 :
        print ("simpang 1, LOW")
        #GPIO.output(23, HIGH)
    if int(count_1) > 0 and int(count_1) <= 2 :
        print("simpang 1, MED")
        #GPIO.output(24, HIGH)
    if int(count_1) > 2:
        print ("simpang 1, HIGH")
        #GPIO.output(25, HIGH)
# simpang 2
    if int(count_2) == 0 :
        print ("simpang 2, LOW")
        #GPIO.output(5, HIGH)
    if int(count_2) > 0 and int(count_1) <= 2 :
        print("simpang 2, MED")
        #GPIO.output(6, HIGH)
    if int(count_2) > 2:
        print ("simpang 2, HIGH")
        #GPIO.output(12, HIGH)
    #simpang 3
    if int(count_3) == 0 :
        print ("simpang 3, LOW")
        #GPIO.output(13, HIGH)
    if int(count_3) > 0 and int(count_1) <= 2 :
        print("simpang 3, MED")
        #GPIO.output(19, HIGH)
    if int(count_3) > 2:
        print ("simpang 3, HIGH")
        #GPIO.output(16, HIGH)
    #simpang 4
    if int(count_4) == 0 :
        print ("simpang 4, LOW")
        #GPIO.output(26, HIGH)
    if 2 >= int(count_4) > 0 :
        print("simpang 4, MED")
        #GPIO.output(20, HIGH)
    if int(count_4) > 2:
        print ("simpang 4, HIGH")
        #GPIO.output(21, HIGH)

    cv2.imshow("Image",frame)
    #cv2.imshow("image2", area1)
    #plt.imshow(frame)
    #plt.show()
    key = cv2.waitKey(1)
    if key == 27:
        break
#while False :
    #GPIO.cleanup()
#GPIO.cleanup()
cap.release()
cv2.destroyAllWindows()
