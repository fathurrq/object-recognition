import socketio
import mysql.connector
import schedule
import time
import uuid
from datetime import date
from datetime import datetime

sio = socketio.Server(cors_allowed_origins='*')
app = socketio.WSGIApp(sio, static_files={
    '/': './public/'
})
mydb = mysql.connector.connect(
  host="localhost",
  user="fathurrq",
  password="@Bungur24",
  database='tss'
)
count = 0
today = date.today()
now = datetime.now().hour
# d4 = today.strftime("%b-%d-%Y")
time_now = today.strftime("%H:%M:%S")

mycursor = mydb.cursor()
def job():
    sql = "INSERT INTO count ( date, hour, count) VALUES ( %s,%s, %s)"
    val = ( today, time_now, count )
    mycursor.execute(sql, val)
    mydb.commit()
    print(mycursor.rowcount, "record inserted.")
schedule.every(1).seconds.do(job)

while 1:
    schedule.run_pending()
    time.sleep(1)

@sio.event
def connect(sid, environ):
    print(sid, "Connected")

@sio.event
def disconnect(sid):
    print(sid, "disconnected")

@sio.event
def count(sid, object_count):
    count = object_count
    print(sid,count)
    sio.emit('count_data', count)

@sio.event
def bus_count(sid, bus_count):
    count = bus_count
    print(sid,count)
    sio.emit('bike_data', count)

@sio.event
def car_count(sid, car_count):
    count = car_count
    print(sid,count)
    sio.emit('car_data', count)
