import socketio

sio = socketio.Server(cors_allowed_origins='*')
app = socketio.WSGIApp(sio, static_files={
    '/': './public/'
})
count = 0
@sio.event
def connect(sid, environ):
    print(sid, "Connected")

@sio.event
def disconnect(sid):
    print(sid, "disconnected")

@sio.event
def count(sid, object_count):
    global count
    count = object_count
    print(sid,count)
    sio.emit('count_data', count)
